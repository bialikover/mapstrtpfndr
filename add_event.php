<?php
include_once "header.php";

// This is used to submit new markers for review.
// Markers won't appear on the map until they are approved.

$title = parseInput($_POST['title']);
$organizer_name = parseInput($_POST['organizer_name']);
$uri = parseInput($_POST['event_uri']);
$start_date = parseInput($_POST['start']);
$end_date = parseInput($_POST['end']);
$address = parseInput($_POST['e_address']);
$lat = parseInput($_POST['e_lat']);
$lng = parseInput($_POST['e_lng']);
$description = parseInput($_POST['e_description']);
$created = time();
// validate fields
if(empty($title) || empty($organizer_name) || empty($uri) || empty($start_date) || empty($end_date) || empty($description)) {
  echo "Todos los campos son requeridos - intenta de nuevo";
  exit;
  
} else {
  
  
  
  // if startup genome mode enabled, post new data to API
  if($sg_enabled) {
    
    try {
      @$r = $http->doPost("/organization", $_POST);
      $response = json_decode($r, 1);
      if ($response['response'] == 'success') {
        include_once("startupgenome_get.php");
        echo "success"; 
        exit;
      }
    } catch (Exception $e) {
      echo "<pre>";
      print_r($e);
    }
    
    
  // normal mode enabled, save new data to local db
  } else {

    // insert into db, wait for approval
    $insert = mysql_query("INSERT INTO events (approved, title, created, organizer_name, description, uri, start_date, end_date, lat, lng, address) VALUES ('1', '$title', '$created', '$organizer_name', '$description', '$uri', '$start_date', '$end_date', '$lat', '$lng', '$address')") or die(mysql_error());

    // geocode new submission
    //$hide_geocode_output = true;
    //include "geocode.php";
    
    echo "success";
    exit;
  
  }

  
}


?>
