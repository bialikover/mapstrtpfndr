<?php
include "header.php";


if(isset($_GET['place_id'])) {
  $place_id = htmlspecialchars($_GET['place_id']); 
} else if(isset($_POST['place_id'])) {
  $place_id = htmlspecialchars($_POST['place_id']);
} else {
  exit; 
}


// get place info
$place_query = mysql_query("SELECT * FROM places WHERE id='$place_id' LIMIT 1");
if(mysql_num_rows($place_query) != 1) { exit; }
$place = mysql_fetch_assoc($place_query);


// do place edit if requested
if($task == "doedit") {
  $title = $_POST['title'];
  $type = $_POST['type'];
  $address = $_POST['address'];
  $uri = $_POST['uri'];
  $description = $_POST['description'];
  $owner_name = $_POST['owner_name'];
  $owner_email = $_POST['owner_email'];
  $type = $_POST['type'];
  
  mysql_query("UPDATE places SET title='$title', type='$type', address='$address', uri='$uri', lat='', lng='', description='$description', owner_name='$owner_name', owner_email='$owner_email' WHERE id='$place_id' LIMIT 1") or die(mysql_error());
  
  // geocode
  $hide_geocode_output = true;
  include "../geocode.php";
  
  header("Location: index.php?view=$view&search=$search&p=$p");
  
  exit;
}

?>



<? echo $admin_head; ?>







<form id="admin" class="form-horizontal" action="edit.php" method="post">
  <h1>
    Editar
  </h1>
  <fieldset>
    <div class="control-group">
      <label class="control-label" for="">Titulo</label>
      <div class="controls">
        <input type="text" class="input input-xlarge" name="title" value="<?=$place['title']?>" id="">
      </div>
    </div>
    <div class="control-group">
      <label class="control-label" for="">Tipo</label>
      <div class="controls">
        <select class="input input-xlarge" name="type">
          <option<? if($place['type'] == "startup") {?> selected="selected"<? } ?>>startup</option>
          <option<? if($place['type'] == "accelerator") {?> selected="selected"<? } ?>>accelerator</option>
          <option<? if($place['type'] == "incubator") {?> selected="selected"<? } ?>>incubator</option>
          <option<? if($place['type'] == "coworking") {?> selected="selected"<? } ?>>coworking</option>
          <option<? if($place['type'] == "investor") {?> selected="selected"<? } ?>>investor</option>
          <option<? if($place['type'] == "service") {?> selected="selected"<? } ?>>service</option>
          <option<? if($place['type'] == "hackerspace") {?> selected="selected"<? } ?>>hackerspace</option>
          <option<? if($place['type'] == "user") {?> selected="selected"<? } ?>>User</option>
        </select>
      </div>
    </div>
    <div class="control-group">
      <label class="control-label" for="">Dirección</label>
      <div class="controls">
        <input type="text" class="input input-xlarge" name="address" value="<?=$place['address']?>" id="">
      </div>
    </div>

    <div class="control-group">
      <label class="control-label" for="">Mapa</label>
      <div class="controls">
        <div id="map" style="width: 400px; height: 300px" class="map_upload"></div>
        <div id="mapcanvas" style="width: 550px; height: 300px; display:none" class="map_upload"></div>
      </div>
    </div>    

    <div class="control-group">
      <label class="control-label" for="">URL</label>
      <div class="controls">
        <input type="text" class="input input-xlarge" name="uri" value="<?=$place['uri']?>" id="">
      </div>
    </div>
    <div class="control-group">
      <label class="control-label" for="">Descripción</label>
      <div class="controls">
        <textarea class="input input-xlarge" name="description"><?=$place['description']?></textarea>
      </div>
    </div>
    <div class="control-group">
      <label class="control-label" for="">Nombre del registrante</label>
      <div class="controls">
        <input type="text" class="input input-xlarge" name="owner_name" value="<?=$place['owner_name']?>" id="">
      </div>
    </div>
    <div class="control-group">
      <label class="control-label" for="">Email del registrante</label>
      <div class="controls">
        <input type="text" class="input input-xlarge" name="owner_email" value="<?=$place['owner_email']?>" id="">
      </div>
    </div>
    <div class="form-actions">
      <button type="submit" class="btn btn-primary">Guardar cambios</button>
      <input type="hidden" name="task" value="doedit" />
      <input type="hidden" name="place_id" value="<?=$place['id']?>" />
      <input type="hidden" name="view" value="<?=$view?>" />
      <input type="hidden" name="search" value="<?=$search?>" />
      <input type="hidden" name="p" value="<?=$p?>" />
      <a href="index.php" class="btn" style="float: right;">Cancelar</a>
    </div>
  </fieldset>
</form>



<script>
$(document).ready(function(){
var lat = <?=$place['lat'];?>;
var lng = <?=$place['lng'];?>;
var markersArray = [];

map = new google.maps.Map(document.getElementById("map"),
  {
    zoom: 15,
    center: new google.maps.LatLng(lat, lng),
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    streetViewControl: false
  }
);
var marker = new google.maps.Marker({
  position: new google.maps.LatLng(lat, lng),
});
marker.setMap(map);


function cargarMapa(){
        var mapOptions = {
                zoom: 15,
                center: new google.maps.LatLng(lat, lng),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                streetViewControl: false
            }
            map2 = new google.maps.Map(document.getElementById("mapcanvas"), mapOptions);
            var options = { componentRestrictions: {country: 'mx'} };        
        google.maps.event.addListener(map2, 'click', function(event) {
                clearOverlays();
                placeMarker(event.latLng);
                updateFormLocation(event.latLng);
        });
    };

    // Update form attributes with given coordinates
    function updateFormLocation(latLng) {
        $('#user_latitude').val(latLng.lat());
        $('#user_longitude').val(latLng.lng());
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({'latLng':latLng}, function(result, status){
                if(status == google.maps.GeocoderStatus.OK){
                        $("#proposition_direction").val(result[0].formatted_address);
                }else{
                        console.log("Geocoder failed due to: " + status);}
                });
    }

    // Add a marker with an open infowindow
    function placeMarker(latLng) {
        console.log("posi: "+latLng); 
        var marker = new google.maps.Marker({
                position: latLng,
                map: map2,
                draggable: true
        });
        markersArray.push(marker);
       // Set and open infowindow
        var infowindow = new google.maps.InfoWindow({
        content: '<div class="popup"><h4>¡Aquí te encuentras!</h4><p>Puedes arrastar el icono para cambiar la ubicación</p>'
        });
        infowindow.open(map2, marker);
        // Listen to drag & drop
        google.maps.event.addListener(marker, 'dragend', function() {
                updateFormLocation(this.getPosition());
        });
        }
        // Removes the overlays from the map
        function clearOverlays() {
                if (markersArray) {
                        for (var i = 0; i < markersArray.length; i++ ) {
                                markersArray[i].setMap(null);
                        }
                }
                markersArray.length = 0;
        }        
        
      });



</script>
<? echo $admin_foot; ?>


