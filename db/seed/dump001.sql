-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 11, 2013 at 02:41 AM
-- Server version: 5.5.25
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `represent`
--

-- --------------------------------------------------------

--
-- Table structure for table `places`
--

CREATE TABLE `places` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `approved` int(1) DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  `kind` varchar(60) NOT NULL,
  `type` varchar(20) NOT NULL,
  `lat` float NOT NULL,
  `lng` float NOT NULL,
  `address` varchar(200) DEFAULT NULL,
  `uri` varchar(200) NOT NULL,
  `description` varchar(255) NOT NULL,
  `sector` varchar(50) NOT NULL,
  `owner_name` varchar(100) NOT NULL,
  `owner_email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=786 ;

--
-- Dumping data for table `places`
--

INSERT INTO `places` (`id`, `approved`, `title`, `kind`, `type`, `lat`, `lng`, `address`, `uri`, `description`, `sector`, `owner_name`, `owner_email`) VALUES
(782, 1, 'victordavid', '', 'design', 19.4377, -99.1356, 'Republica de Chile #30, Centro, Mexico, DF, ', 'blog.victordavid.me', 'Soy diseÃ±ador', '', 'Victor David Mejia', 'i7avi7@gmail.com'),
(783, NULL, 'suarezj', '', 'user', 0, 0, 'Una de prueba', 'unodeprueba.com', 'dsnfklsdnfkl', 'communication', 'julian suarez', 'suarezj@gmail.com'),
(784, 1, 'adan', 'user', 'user', 19.4385, -99.1147, '', 'http://twitter.com/bialikover', 'I&#039;M RoR developer trying to get things done', 'programming', 'Adan', 'adan@startupfinder.org'),
(785, 1, 'startupfinder', 'company', 'startup', 19.4346, -99.1956, '', 'startupfinder.org', 'startup', '8', 'adan', 'adan@startupfinder.org');
