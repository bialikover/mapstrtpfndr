<?php
include_once "header.php";
?>

<!DOCTYPE html>
<html>
  <head>
    <!--
    This site was based on the Represent.LA project by:
    - Alex Benzer (@abenzer)
    - Tara Tiger Brown (@tara)
    - Sean Bonner (@seanbonner)
    
    Create a map for your startup community!
    https://github.com/abenzer/represent-map
    -->
    <title>Startup finder</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta charset="UTF-8">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700|Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link href="./bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="./bootstrap/css/bootstrap-responsive.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="map.css?nocache=289671982568" type="text/css" />
    <link rel="stylesheet" href="./scripts/datetimepicker.css" type="text/css" />
    <link rel="stylesheet" media="only screen and (max-device-width: 480px)" href="mobile.css" type="text/css" />
    <script src="./scripts/jquery-1.7.1.js" type="text/javascript" charset="utf-8"></script>
    <script src="./scripts/sf.js" type="text/javascript" charset="utf-8"></script>
    <script src="./bootstrap/js/bootstrap.js" type="text/javascript" charset="utf-8"></script>
  <script src="./scripts/bootstrap-datetimepicker.js"></script>
  <script src="./scripts/locales/bootstrap-datetimepicker.es.js"></script>

    <script src="./bootstrap/js/bootstrap-typeahead.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="./scripts/label.js"></script>
    
    <script type="text/javascript">
      var map;
      var infowindow = null;
      var gmarkers = [];
      var markerTitles =[];
      var highestZIndex = 0;  
      var agent = "default";
      var zoomControl = true;


      // detect browser agent
      $(document).ready(function(){
        if(navigator.userAgent.toLowerCase().indexOf("iphone") > -1 || navigator.userAgent.toLowerCase().indexOf("ipod") > -1) {
          agent = "iphone";
          zoomControl = false;
        }
        if(navigator.userAgent.toLowerCase().indexOf("ipad") > -1) {
          agent = "ipad";
          zoomControl = false;
        }
      }); 
      

      // resize marker list onload/resize
      $(document).ready(function(){
        resizeList() 
      });
      $(window).resize(function() {
        resizeList();
      });
      
      // resize marker list to fit window
      function resizeList() {
        newHeight = $('html').height() - $('#topbar').height();
        $('#list').css('height', newHeight + "px"); 
        $('#menu').css('margin-top', $('#topbar').height()); 
      }


      // initialize map
      function initialize() {
        // set map styles
        var mapStyles = [
         {
            featureType: "road",
            elementType: "geometry",
            stylers: [
              { hue: "#8800ff" },
              { lightness: 100 }
            ]
          },{
            featureType: "road",
            stylers: [
              { visibility: "on" },
              { hue: "#91ff00" },
              { saturation: -62 },
              { gamma: 1.98 },
              { lightness: 45 }
            ]
          },{
            featureType: "water",
            stylers: [
              { hue: "#005eff" },
              { gamma: 0.72 },
              { lightness: 42 }
            ]
          },{
            featureType: "transit.line",
            stylers: [
              { visibility: "off" }
            ]
          },{
            featureType: "administrative.locality",
            stylers: [
              { visibility: "on" }
            ]
          },{
            featureType: "administrative.neighborhood",
            elementType: "geometry",
            stylers: [
              { visibility: "simplified" }
            ]
          },{
            featureType: "landscape",
            stylers: [
              { visibility: "on" },
              { gamma: 0.41 },
              { lightness: 46 }
            ]
          },{
            featureType: "administrative.neighborhood",
            elementType: "labels.text",
            stylers: [
              { visibility: "on" },
              { saturation: 33 },
              { lightness: 20 }
            ]
          }
        ];

        // set map options
        var myOptions = {
          zoom: 11,
          //minZoom: 10,
          center: new google.maps.LatLng(19.432608,-99.133208),
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          streetViewControl: false,
          mapTypeControl: false,
          panControl: false,
          zoomControl: zoomControl,
          styles: mapStyles,
          zoomControlOptions: {
            style: google.maps.ZoomControlStyle.SMALL,
            position: google.maps.ControlPosition.LEFT_CENTER
          }
        };
        map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
        zoomLevel = map.getZoom();

        // prepare infowindow
        infowindow = new google.maps.InfoWindow({
          content: "holding..."
        });

        // only show marker labels if zoomed in
        google.maps.event.addListener(map, 'zoom_changed', function() {
          zoomLevel = map.getZoom();
          if(zoomLevel <= 15) {
            $(".marker_label").css("display", "none");
          } else {
            $(".marker_label").css("display", "inline");
          }
        });

        // markers array: name, type (icon), lat, long, description, uri, address
        markers = new Array();
        <?php
          $types = Array(
              Array('startup', 'Startups'),
              Array('accelerator','Accelerators'),
              Array('incubator', 'Incubators'), 
              Array('coworking', 'Coworking'), 
              Array('investor', 'Investors'),
              Array('service', 'Consulting'),
              Array('hackerspace', 'Hackerspaces'),
              Array('event', 'Events'),
              Array('user', 'Users')
              );
          $marker_id = 0;
          foreach($types as $type) {
            $places = mysql_query("SELECT * FROM places WHERE approved='1' AND type='$type[0]' ORDER BY title");
            $places_total = mysql_num_rows($places);
            $count = array(
              'startup' => 0,
'accelerator' => 0,
'incubator' => 0,
'coworking' => 0,
'investor' => 0,
'service' => 0,
'hackerspace' => 0,
'event' => 0,
'user' => 0
              );
            while($place = mysql_fetch_assoc($places)) {
              $place['title'] = htmlspecialchars_decode(addslashes(htmlspecialchars($place['title'])));
              $place['description'] = htmlspecialchars_decode(addslashes(htmlspecialchars($place['description'])));
              $place['uri'] = addslashes(htmlspecialchars($place['uri']));
              $place['address'] = htmlspecialchars_decode(addslashes(htmlspecialchars($place['address'])));
              echo "
                markers.push(['".$place['title']."', '".$place['type']."', '".$place['lat']."', '".$place['lng']."', '".$place['description']."', '".$place['uri']."', '".$place['address']."']); 
                markerTitles[".$marker_id."] = '".$place['title']."';
              "; 
              $count[$place['type']]++;
              $marker_id++;
            }
          } 
          if($show_events == true) {
            $place['type'] = "event";
            $events = mysql_query("SELECT * FROM events WHERE start_date > ".time()." AND start_date < ".(time()+4838400)." ORDER BY id DESC");
            $events_total = mysql_num_rows($events);
            while($event = mysql_fetch_assoc($events)) {
              $event[title] = htmlspecialchars_decode(addslashes(htmlspecialchars($event[title])));
              $event[description] = htmlspecialchars_decode(addslashes(htmlspecialchars($event[description])));
              $event[uri] = addslashes(htmlspecialchars($event[uri]));
              $event[address] = htmlspecialchars_decode(addslashes(htmlspecialchars($event[address])));
              $event[start_date] = date("D, M j @ g:ia", $event[start_date]);
              echo "
                markers.push(['".$event[title]."', 'event', '".$event[lat]."', '".$event[lng]."', '".$event[start_date]."', '".$event[uri]."', '".$event[address]."']); 
                markerTitles[".$marker_id."] = '".$event[title]."';
              "; 
              $count[$place['type']]++;
              $marker_id++;
            }
          }
        ?>

        // add markers
        jQuery.each(markers, function(i, val) {
          infowindow = new google.maps.InfoWindow({
            content: ""
          });

          // offset latlong ever so slightly to prevent marker overlap
          rand_x = Math.random();
          rand_y = Math.random();
          val[2] = parseFloat(val[2]) + parseFloat(parseFloat(rand_x) / 6000);
          val[3] = parseFloat(val[3]) + parseFloat(parseFloat(rand_y) / 6000);

          // show smaller marker icons on mobile
          if(agent == "iphone") {
            var iconSize = new google.maps.Size(16,19);
          } else {
            iconSize = null;
          }

          // build this marker
          var markerImage = new google.maps.MarkerImage("./images/icons/"+val[1]+".png", null, null, null, iconSize);
          var marker = new google.maps.Marker({
            position: new google.maps.LatLng(val[2],val[3]),
            map: map,
            title: '',
            clickable: true,
            infoWindowHtml: '',
            zIndex: 10 + i,
            icon: markerImage
          });
          marker.type = val[1];
          gmarkers.push(marker);

          // add marker hover events (if not viewing on mobile)
          if(agent == "default") {
            google.maps.event.addListener(marker, "mouseover", function() {
              this.old_ZIndex = this.getZIndex(); 
              this.setZIndex(9999); 
              $("#marker"+i).css("display", "inline");
              $("#marker"+i).css("z-index", "99999");
            });
            google.maps.event.addListener(marker, "mouseout", function() { 
              if (this.old_ZIndex && zoomLevel <= 15) {
                this.setZIndex(this.old_ZIndex); 
                $("#marker"+i).css("display", "none");
              }
            }); 
          }

          // format marker URI for display and linking
          var markerURI = val[5];
          if(markerURI.substr(0,7) != "http://") {
            markerURI = "http://" + markerURI; 
          }
          var markerURI_short = markerURI.replace("http://", "");
          var markerURI_short = markerURI_short.replace("www.", "");

          // add marker click effects (open infowindow)
          google.maps.event.addListener(marker, 'click', function () {
            infowindow.setContent(
              "<div class='marker_title'>"+val[0]+"</div>"
              + "<div class='marker_uri'><a target='_blank' href='"+markerURI+"'>"+markerURI_short+"</a></div>"
              + "<div class='marker_desc'>"+val[4]+"</div>"
              + "<div class='marker_address'>"+val[6]+"</div>"
            );
            infowindow.open(map, this);
          });

          // add marker label
          var latLng = new google.maps.LatLng(val[2], val[3]);
          var label = new Label({
            map: map,
            id: i
          });
          label.bindTo('position', marker);
          label.set("text", val[0]);
          label.bindTo('visible', marker);
          label.bindTo('clickable', marker);
          label.bindTo('zIndex', marker);
        });


        // zoom to marker if selected in search typeahead list
        $('#search').typeahead({
          source: markerTitles, 
          onselect: function(obj) {
            marker_id = jQuery.inArray(obj, markerTitles);
            if(marker_id > -1) {
              map.panTo(gmarkers[marker_id].getPosition());
              map.setZoom(15);
              google.maps.event.trigger(gmarkers[marker_id], 'click');
            }
            $("#search").val("");
          }
        });
      } 


      // zoom to specific marker
      function goToMarker(marker_id) {
        if(marker_id) {
          map.panTo(gmarkers[marker_id].getPosition());
          map.setZoom(15);
          google.maps.event.trigger(gmarkers[marker_id], 'click');
        }
      }

      // toggle (hide/show) markers of a given type (on the map)
      function toggle(type) {
        if($('#filter_'+type).is('.inactive')) {
          show(type); 
        } else {
          hide(type); 
        }
      }

      // hide all markers of a given type
      function hide(type) {
        for (var i=0; i<gmarkers.length; i++) {
          if (gmarkers[i].type == type) {
            gmarkers[i].setVisible(false);
          }
        }
        $("#filter_"+type).addClass("inactive");
      }

      // show all markers of a given type
      function show(type) {
        for (var i=0; i<gmarkers.length; i++) {
          if (gmarkers[i].type == type) {
            gmarkers[i].setVisible(true);
          }
        }
        $("#filter_"+type).removeClass("inactive");
      }
      
      // toggle (hide/show) marker list of a given type
      function toggleList(type) {
        $("#list .list-"+type).toggle();
      }


      // hover on list item
      function markerListMouseOver(marker_id) {
        $("#marker"+marker_id).css("display", "inline");
      }
      function markerListMouseOut(marker_id) {
        $("#marker"+marker_id).css("display", "none");
      }

      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    
    <? echo $head_html; ?>
  </head>
  <body>
    
    <!-- display error overlay if something went wrong -->
    <?php echo $error; ?>
    
    <!-- facebook like button code -->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=421651897866629";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    
    <!-- google map -->
    <div id="map_canvas"></div>
    
    <!-- topbar -->
    <div class="topbar" id="topbar">
      <div class="wrapper">
        <div class="right">
          <div class="share">
            <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.startupfinder.org" data-text="Conoce el ecosistema emprendedor en méxico" data-via="startup_finder" data-count="none">Tweet</a>
            <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
            <div class="fb-like" data-href="http://www.startupfinder.org" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false" data-font="arial"></div>
          </div>
        </div>
        <div class="left">
          <div class="logo">
            <a href="./">
              <img src="images/startupfinder.png" alt="" />
            </a>
          </div>
          <div class="buttons">
            <!--<a href="#modal_info" class="btn btn-large btn-info" data-toggle="modal"><i class="icon-info-sign icon-white"></i>About this Map</a>-->
            <?php if($sg_enabled) { ?>
              <a href="#modal_add_choose" class="btn btn-large btn-success" data-toggle="modal"><i class="icon-plus-sign icon-white"></i>Add Something</a>
            <? } else { ?>
              <a href="#modal_add" class="btn btn-large btn-success" data-toggle="modal"><i class="icon-plus-sign icon-white"></i>Sumate al mapa</a>
              <a href="#modal_add2" class="btn btn-large btn-success" data-toggle="modal"><i class="icon-plus-sign icon-white"></i>Agrega un evento</a>
            <? } ?>
          </div>
          <div class="search">
            <input type="text" name="search" id="search" placeholder="Buscar..." data-provide="typeahead" autocomplete="off" />
          </div>
        </div>
      </div>
    </div>
    
    <!-- right-side gutter -->
    <div class="menu" id="menu">
      <ul class="list" id="list">
        <?php
          $types = Array(
              Array('startup', 'Startups'),
              Array('accelerator','Aceleradora'),
              Array('incubator', 'Incubadora'), 
              Array('coworking', 'Coworking'), 
              Array('investor', 'Inversionista'),
              Array('service', 'Consultoria'),
              Array('hackerspace', 'Hackerspaces'),
              Array('user', 'Usuarios')
              );
          if($show_events == true) {
            $types[] = Array('event', 'Eventos'); 
          }
          $marker_id = 0;
          foreach($types as $type) {
            if($type[0] != "event") {
              $markers = mysql_query("SELECT * FROM places WHERE approved='1' AND type='$type[0]' ORDER BY title");
            } else {
              $markers = mysql_query("SELECT * FROM events WHERE start_date > ".time()." AND start_date < ".(time()+4838400)." ORDER BY id DESC");
            }
            $markers_total = mysql_num_rows($markers);
            echo "
              <li class='category'>
                <div class='category_item'>
                  <div class='category_toggle' onClick=\"toggle('$type[0]')\" id='filter_$type[0]'></div>
                  <a href='#' onClick=\"toggleList('$type[0]');\" class='category_info'><img src='./images/icons/$type[0].png' alt='' />$type[1]<span class='total'> ($markers_total)</span></a>
                </div>
                <ul class='list-items list-$type[0]'>
            ";
            while($marker = mysql_fetch_assoc($markers)) {
              echo "
                  <li class='".$marker['type']."'>
                    <a href='#' onMouseOver=\"markerListMouseOver('".$marker_id."')\" onMouseOut=\"markerListMouseOut('".$marker_id."')\" onClick=\"goToMarker('".$marker_id."');\">".$marker['title']."</a>
                  </li>
              ";
              $marker_id++;
            }
            echo "
                </ul>
              </li>
            ";
          }
        ?>
        <li class="blurb">
          Este mapa se ha hecho para localizar el movimiento emprendedor en México.
        </li>
        <li class="attribution">
          <!-- per our license, you may not remove this line -->
          <?=$attribution?>
        </li>
      </ul>
    </div>
    
    <!-- more info modal -->
    <div class="modal hide" id="modal_info">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>About this Map</h3>
      </div>
      <div class="modal-body">
        <p>
          We built this map to connect and promote the tech startup community
          in our beloved Los Angeles. We've seeded the map but we need
          your help to keep it fresh. If you don't see your company, please 
          <?php if($sg_enabled) { ?>
            <a href="#modal_add_choose" data-toggle="modal" data-dismiss="modal">submit it here</a>.
          <?php } else { ?>
            <a href="#modal_add" data-toggle="modal" data-dismiss="modal">submit it here</a>.
          <?php } ?>
          Let's put LA on the map together!
        </p>
        <p>
          Questions? Feedback? Connect with us: <a href="http://www.twitter.com/startup_finder" target="_blank">@startup_finder</a>
        </p>
        <p>
          If you want to support the LA community by linking to this map from your website,
          here are some badges you might like to use. You can also grab the <a href="./images/badges/LA-icon.ai">LA icon AI file</a>.
        </p>
        <ul class="badges">
          <li>
            <img src="./images/badges/badge1.png" alt="">
          </li>
          <li>
            <img src="./images/badges/badge1_small.png" alt="">
          </li>
          <li>
            <img src="./images/badges/badge2.png" alt="">
          </li>
          <li>
            <img src="./images/badges/badge2_small.png" alt="">
          </li>
        </ul>
        <p>
          This map was built with <a href="https://github.com/abenzer/represent-map">RepresentMap</a> - an open source project we started
          to help startup communities around the world create their own maps. 
          Check out some <a target="_blank" href="http://www.representmap.com">startup maps</a> built by other communities!
        </p>
      </div>
      <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal" style="float: right;">Close</a>
      </div>
    </div>
    
    
    <!-- add something modal -->
    <div class="modal hide" id="modal_add">
      <form action="add.php" id="modal_addform" class="form-horizontal">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h3>Sumate!</h3>
        </div>
        <div class="modal-body">
          <div id="result"></div>
          <fieldset>
            <div id="step1">
              <h2>Paso 1 Información básica</h2>
            <div class="control-group">
              <label class="control-label" for="add_owner_name">Nombre</label>
              <div class="controls">
                <input type="text" class="input-xlarge" name="owner_name" id="add_owner_name" maxlength="100">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="add_owner_email">Email</label>
              <div class="controls">
                <input type="text" class="input-xlarge" name="owner_email" id="add_owner_email" maxlength="100">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="kind">¿que estas registrando?</label>              
              <div class="controls">
                <p><input type="radio" name="kind" id="add_kind" value="user" checked/> Mi Perfil de usuario
                <input type="radio" name="kind" id="add_kind" value="company" /> Mi Compañia</p>
              </div>
            </div>
            <div id="user" >
              <div class="control-group">
                <label class="control-label" for="add_title">usuario</label>
                <div class="controls">
                  <b>http://startupfinder.org/</b><input type="text" name="title" id="add_title" maxlength="16" autocomplete="off">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="input01">Área en la que te desarrollas</label>
                <div class="controls">
                  <select name="type" id="add_type" class="input-xlarge">
                    <option value="design">Diseño</option>
                    <option value="marketing">Mercadotecnia</option>
                    <option value="comunication">Comunicación</option>
                    <option value="programming">Programación</option>
                    <option value="electronic">Electrónica</option>
                    <option value="business">Negocios</option>
                    <option value="law">Leyes</option>
                  </select>
                </div>
              </div> 
              <div class="control-group">
              <label class="control-label" for="add_description">Descripción</label>
              <div class="controls">
                <input type="text" class="input-xlarge" id="add_description" name="description" maxlength="150" placeholder="soy UX designer buscando cofounder para startup...">
                <p class="help-block">
                  ¿Cómo te describes? (que haces, que te gusta, etc) Máx. 150 caracteres.
                </p>
              </div>
            </div>               
            </div>
            <div id="company" style="display:none">
              <div class="control-group">
               <label class="control-label" for="add_title">Compañia</label>
                <div class="controls">
                  <b>http://startupfinder.org/</b><input type="text" name="title" id="add_title_c" maxlength="16" autocomplete="off">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="input01">Tipo de compañia</label>
                <div class="controls">
                  <select name="type" id="add_type_c" class="input-xlarge">
                    <option value="startup">Startup</option>
                    <option value="accelerator">Acceleradora</option>
                    <option value="incubator">Incubadora</option>
                    <option value="coworking">Coworking</option>
                    <option value="investor">VC/Angel</option>
                    <option value="service">Compañia de consultoria</option>
                    <option value="hackerspace">Hackerspace</option>
                  </select>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label" for="input05">Sector de tu compañia</label>
                <div class="controls">
                  <select name="type" id="add_sector_c" class="input-xlarge">
<option value="1">Administrativos</option>
<option value="2">Biología</option>
<option value="3">Comunicaciones</option>
<option value="5">Construcción</option>
<option value="4">Contabilidad</option>
<option value="6">Creatividad, Producción y Diseño Comercial</option>
<option value="7">Derecho y Leyes</option>
<option value="8">Educación</option>
<option value="9">Ingeniería</option>
<option value="10">Logística, Transportación y Distribución</option>
<option value="11">Manufactura, Producción y Operación</option>
<option value="12">Mercadotecnia, Publicidad y Relaciones Públicas</option>
<option value="13">Recursos Humanos</option>
<option value="14">Salud y Belleza</option>
<option value="15">Sector Salud</option>
<option value="16">Seguro y Reaseguro</option>
<option value="17">Tecnologías de la Información / Sistemas</option>
<option value="18">Turismo, Hospitalidad y Gastronomía</option>
<option value="19">Ventas </option>
<option value="20">Veterinaria / Zoología</option>

                  </select>
                </div>
              </div>
              <div class="control-group">
              <label class="control-label" for="add_description">Descripción</label>
              <div class="controls">
                <input type="text" class="input-xlarge" id="add_description_c" name="description" maxlength="150">
                <p class="help-block">
                  ¿Qué hace tu producto?, ¿Qué problema estas resolviendo? Máx. 150 caracteres.
                </p>
              </div>
            </div>  
            </div>
          </div>
          <div id="step2" style="display:none">
            <h2>Paso 2 Indica tu ubicación</h2>
            <div class="control-group">
              <label class="control-label" for="add_address">Dirección</label></br>
              <input type="text" class="input-xlarge" name="address" id="address">                              
                <div id="map_canvas_set" class="map_upload"></div>
                <input type="hidden" class="input-xlarge" name="user_latitude" id="user_latitude">
                <input type="hidden" class="input-xlarge" name="user_longitude" id="user_longitude">                              
                
            </div>
            <div class="control-group">
              <label class="control-label" for="add_uri">Website URL</label>
              <div class="controls">
                <input type="text" class="input-xlarge" id="add_uri" name="uri" placeholder="http://">
                <p class="help-block">
                 La dirección completa de la compañia ejemplo: "http://www.yoursite.com"
                </p>
              </div>
            </div>
          </div>          
          </fieldset>
        </div>
        <div class="modal-footer">                    
          <a href="#" class="btn" id="prev" style="display:none;">Atras</a>
          <a href="#" class="btn" id="next" style="float: right;">Siguiente</a>
          <button type="submit" id="send" class="btn btn-primary" style="float:right; display:none">Enviar</button>          
        </div>
      </form>
    </div>
    <script>
      // add modal form submit
      $("#next").click(function(){

        $("#step1").hide();
        $("#step2").show();
        var latitudeSet = $("#user_latitude").val();
        if(! latitudeSet){
          cargarMapa();
        }
        $("#prev").show();
        $(this).hide();
        $("#send").show();

      });

      $("#prev").click(function(){

        $("#step2").hide();
        $("#step1").show();        
        $("#next").show();
        $(this).hide();
        $("#send").hide();

      });


        // Goelocalización y demás
         var lat = 19.432595422657403;
        var lng = -99.13336157798767;
        var markersArray = [];
    // On click, clear markers, place a new one, update coordinates in the form
    function cargarMapa(){
        var mapOptions = {
                zoom: 15,
                center: new google.maps.LatLng(lat, lng),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                streetViewControl: false
            }
            map2 = new google.maps.Map(document.getElementById("map_canvas_set"), mapOptions);
            var options = { componentRestrictions: {country: 'mx'} };
            //autocomplete = new google.maps.places.Autocomplete($(".search-dir")[0], options);
            //geocoder = new google.maps.Geocoder();
            /*google.maps.event.addListener(autocomplete, 'place_changed', function() {
            //var place = autocomplete.getPlace();
            //console.log(place.formatted_address);
            /*geocoder.geocode({"address": place.formatted_address }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    lat = results[0].geometry.location.lat(),
                    lng = results[0].geometry.location.lng(),
                    placeName = results[0].address_components[0].long_name,
                    myLatlng = new google.maps.LatLng(lat, lng);
                    clearOverlays();
                    placeMarker(myLatlng);
                                        updateFormLocation(myLatlng);
                }
            });
        });*/
        if(navigator.geolocation){   
                        navigator.geolocation.getCurrentPosition(
                                function(position){
                                        lat = position.coords.latitude;
                                        lng = position.coords.longitude;                                        
                                        var myLatlng = new google.maps.LatLng(lat,lng);
                                        console.log("lat: "+lat+" y "+" log: "+lng);
                                        placeMarker(myLatlng);
                                        updateFormLocation(myLatlng);
                                },
                                function(){
                                        alert("Tu ubicación no está disponible, usando ubicación predeterminada.");                                        
                                        var myLatlng = new google.maps.LatLng(lat,lng);
                                        console.log("lat: "+lat+" y "+" log: "+lng);
                                        placeMarker(myLatlng);
                                        updateFormLocation(myLatlng);
                                }, {timeout:5000});
                } else{
                        alert("Tu navegador no soporta geolocalización");
                }
        google.maps.event.addListener(map2, 'click', function(event) {
                clearOverlays();
                placeMarker(event.latLng);
                updateFormLocation(event.latLng);
        });
    };

    // Update form attributes with given coordinates
    function updateFormLocation(latLng) {
        $('#user_latitude').val(latLng.lat());
        $('#user_longitude').val(latLng.lng());
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({'latLng':latLng}, function(result, status){
                if(status == google.maps.GeocoderStatus.OK){
                        $("#address").val(result[0].formatted_address);
                }else{
                        console.log("Geocoder failed due to: " + status);}
                });
    }

    // Add a marker with an open infowindow
    function placeMarker(latLng) {
        console.log("posi: "+latLng); 
        var marker = new google.maps.Marker({
                position: latLng,
                map: map2,
                draggable: true
        });
        markersArray.push(marker);
       // Set and open infowindow
        var infowindow = new google.maps.InfoWindow({
        content: '<div class="popup"><h4>¡Aquí te encuentras!</h4><p>Puedes arrastar el icono para cambiar la ubicación</p>'
        });
        infowindow.open(map2, marker);
        // Listen to drag & drop
        google.maps.event.addListener(marker, 'dragend', function() {
                updateFormLocation(this.getPosition());
        });
        }
        // Removes the overlays from the map
        function clearOverlays() {
                if (markersArray) {
                        for (var i = 0; i < markersArray.length; i++ ) {
                                markersArray[i].setMap(null);
                        }
                }
                markersArray.length = 0;
        }        

        //cargarMapa();

        //end mapa



      $("#modal_addform").submit(function(event) {
        event.preventDefault(); 
        // get values

        kind = $( "input:radio[name=kind]:checked" ).val();
        if(kind==="user"){
            var $form = $( this ),
            owner_name = $form.find( '#add_owner_name' ).val(),
            owner_email = $form.find( '#add_owner_email' ).val(),                        
            sector = $form.find( '#add_type' ).val(),
            title = $form.find( '#add_title' ).val(),
            type = "user",
            address = $form.find( '#address' ).val(),
            u_latitude = $form.find('#user_latitude').val(),
            u_longitude = $form.find('#user_longitude').val(),
            uri = $form.find( '#add_uri' ).val(),
            description = $form.find( '#add_description' ).val(),
            url = $form.attr( 'action' );
        }
        else{
            var $form = $( this ),
            owner_name = $form.find( '#add_owner_name' ).val(),
            owner_email = $form.find( '#add_owner_email' ).val(),                        
            sector = $form.find('#add_sector_c').val(),
            title = $form.find( '#add_title_c' ).val(),
            type = $form.find( '#add_type_c' ).val(),
            address = $form.find( '#address' ).val(),
            u_latitude = $form.find('#user_latitude').val(),
            u_longitude = $form.find('#user_longitude').val(),
            uri = $form.find( '#add_uri' ).val(),
            description = $form.find( '#add_description_c' ).val(),
            url = $form.attr( 'action' ); 
        }

        console.log("sector: "+sector+", kind: "+kind+", lat: "+u_latitude+", long: "+ u_longitude+", type: "+type+", title: "+title+", uri: "+uri+", description: "+description+", owner_name: "+owner_name+", owner_email: "+owner_email);
            //throw new Error("Error intentionally created to halt process. Not an actual error.");

        // send data and get results

        $.post( url, { owner_name: owner_name,
                       owner_email: owner_email, 
                       title: title, 
                       type: type, 
                       address: address,
                       lat: u_latitude, 
                       lng: u_longitude,
                       uri: uri, 
                       description: description, 
                       kind:kind, 
                       sector:sector },
          function( data ) {
            var content = $( data ).find( '#content' );
            
            // if submission was successful, show info alert
            if(data == "success") {
              $("#modal_addform #result").html("Gracias por sumarte, prontó aparecerás en el mapa"); 
              $("#modal_addform #result").addClass("alert alert-info");
              $("#modal_addform p").css("display", "none");
              $("#modal_addform fieldset").css("display", "none");
              $("#modal_addform .btn-primary").css("display", "none");
              
            // if submission failed, show error
            } else {
              $("#modal_addform #result").html(data); 
              $("#modal_addform #result").addClass("alert alert-danger");
            }
          }
        );
      });
    </script>
      





















    <!-- add something modal -->
    <div class="modal hide" id="modal_add2">
      <form action="add_event.php" id="modal_addform2" class="form-horizontal">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h3>Agrega un evento!</h3>
        </div>
        <div class="modal-body">
          <div id="result"></div>
          <fieldset>
            <div id="eventstep1">
              <h2>Paso 1 Información básica</h2>
            <div class="control-group">
              <label class="control-label" for="add_event_title">Título del evento</label>
              <div class="controls">
                <input type="text" class="input-xlarge" name="add_event_title" id="add_event_title" maxlength="100">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="organizer_namel">Nombre del organizador</label>
              <div class="controls">
                <input type="text" class="input-xlarge" name="organizer_name" id="organizer_name" maxlength="100">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="add_event_uri">URL del evento</label>
              <div class="controls">
                <input type="text" class="input-xlarge" id="add_event_uri" name="add_event_uri" placeholder="http://">
                <p class="help-block">
                 La dirección completa donde puedo encontrar información del evento: "http://www.yoursite.com"
                </p>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="add_event_description">Descripción</label>
              <div class="controls">
                <input type="text" class="input-xlarge" id="add_event_description" name="add_event_description" maxlength="150">
                <p class="help-block">
                  ¿De que se trata el evento?, ¿Qué va haber ahí? Máx. 150 caracteres.
                </p>
              </div>
            </div>
          <div class="control-group">
            <label class="control-label">Fecha de Inicio</label>
            <div class="controls input-append date form_datetime" data-date="<?php echo date("Y-m-d H:i:s");?>" data-date-format="dd MM yyyy - hh:ii" data-link-field="dtp_input1">
                <input size="16" type="text"  readonly>
                 <span class="add-on"><i class="icon-remove"></i></span>
                    <span class="add-on"><i class="icon-th"></i></span>
            </div>
               <input type="hidden" id="dtp_input1"  name ="date_start"/><br/>
          </div>

          <div class="control-group">
            <label class="control-label">Fecha de Termino</label>
            <div class="controls input-append date form_datetime" data-date="<?php echo date("Y-m-d H:i:s");?>" data-date-format="dd MM yyyy - hh:ii" data-link-field="dtp_input2">
                <input size="16" type="text"  readonly>
                 <span class="add-on"><i class="icon-remove"></i></span>
                    <span class="add-on"><i class="icon-th"></i></span>
            </div>
               <input type="hidden" id="dtp_input2" name="date_end"/><br/>
          </div>
            </div>
            <div id="eventstep2" style="display:none">
              <h2>Paso 2 ¿Dónde será el evento?</h2>
              <div class="control-group">
              <label class="control-label" for="event_address">Dirección</label></br>
              <input type="text" class="input-xlarge" name="event_address" id="event_address">                              
                <div id="map_canvas_events" class="map_upload"></div>
                <input type="hidden" class="input-xlarge" name="event_latitude" id="event_latitude">
                <input type="hidden" class="input-xlarge" name="event_longitude" id="event_longitude">                              
                
            </div>
            </div>
          </fieldset>
        </div>
        <div class="modal-footer">                   
          <a href="#" class="btn" id="prev_event" style="display:none;">Atras</a>
          <a href="#" class="btn" id="next_event" style="float: right;">Siguiente</a>
          <button type="submit" id="send_event" class="btn btn-primary" style="float:right; display:none">Enviar</button>         
        </div>
      </form>
    </div>






<script>
    $('.form_datetime').datetimepicker({
        language:  'es',
        weekStart: 1,
        todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    forceParse: 0
    });

     // add modal form submit
      $("#next_event").click(function(){

        $("#eventstep1").hide();
        $("#eventstep2").show();
        var latitudeSet = $("#event_latitude").val();
        if(! latitudeSet){
          cargarMapaEvent();
        }
        $("#prev_event").show();
        $(this).hide();
        $("#send_event").show();

      });

      $("#prev_event").click(function(){

        $("#eventstep2").hide();
        $("#eventstep1").show();        
        $("#next_event").show();
        $(this).hide();
        $("#send_event").hide();

      });


        // Goelocalización y demás
         var lat = 19.432595422657403;
        var lng = -99.13336157798767;
        var markersArray = [];
    // On click, clear markers, place a new one, update coordinates in the form
    function cargarMapaEvent(){
        var mapOptions = {
                zoom: 15,
                center: new google.maps.LatLng(lat, lng),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                streetViewControl: false
            }
            map3 = new google.maps.Map(document.getElementById("map_canvas_events"), mapOptions);
            var options = { componentRestrictions: {country: 'mx'} };
            //autocomplete = new google.maps.places.Autocomplete($(".search-dir")[0], options);
            //geocoder = new google.maps.Geocoder();
            /*google.maps.event.addListener(autocomplete, 'place_changed', function() {
            //var place = autocomplete.getPlace();
            //console.log(place.formatted_address);
            /*geocoder.geocode({"address": place.formatted_address }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    lat = results[0].geometry.location.lat(),
                    lng = results[0].geometry.location.lng(),
                    placeName = results[0].address_components[0].long_name,
                    myLatlng = new google.maps.LatLng(lat, lng);
                    clearOverlays();
                    placeMarker(myLatlng);
                                        updateFormLocation(myLatlng);
                }
            });
        });*/
        if(navigator.geolocation){   
                        navigator.geolocation.getCurrentPosition(
                                function(position){
                                        lat = position.coords.latitude;
                                        lng = position.coords.longitude;                                        
                                        var myLatlng = new google.maps.LatLng(lat,lng);
                                        console.log("lat: "+lat+" y "+" log: "+lng);
                                        placeMarker3(myLatlng);
                                        updateFormLocation3(myLatlng);
                                },
                                function(){
                                        alert("Tu ubicación no está disponible, usando ubicación predeterminada.");                                        
                                        var myLatlng = new google.maps.LatLng(lat,lng);
                                        console.log("lat: "+lat+" y "+" log: "+lng);
                                        placeMarker3(myLatlng);
                                        updateFormLocation3(myLatlng);
                                }, {timeout:5000});
                } else{
                        alert("Tu navegador no soporta geolocalización");
                }
        google.maps.event.addListener(map3, 'click', function(event) {
                clearOverlays3();
                placeMarker3(event.latLng);
                updateFormLocation3(event.latLng);
        });
    };

    // Update form attributes with given coordinates
    function updateFormLocation3(latLng) {
        $('#event_latitude').val(latLng.lat());
        $('#event_longitude').val(latLng.lng());
        geocoder = new google.maps.Geocoder();
        geocoder.geocode({'latLng':latLng}, function(result, status){
                if(status == google.maps.GeocoderStatus.OK){
                        $("#event_address").val(result[0].formatted_address);
                }else{
                        console.log("Geocoder failed due to: " + status);}
                });
    }

    // Add a marker with an open infowindow
    function placeMarker3(latLng) {
        console.log("posi: "+latLng); 
        var marker = new google.maps.Marker({
                position: latLng,
                map: map3,
                draggable: true
        });
        markersArray.push(marker);
       // Set and open infowindow
        var infowindow = new google.maps.InfoWindow({
        content: '<div class="popup"><h4>¡Aquí es el evento!</h4><p>Puedes arrastar el icono para cambiar la ubicación</p>'
        });
        infowindow.open(map3, marker);
        // Listen to drag & drop
        google.maps.event.addListener(marker, 'dragend', function() {
                updateFormLocation3(this.getPosition());
        });
        }
        // Removes the overlays from the map
        function clearOverlays3() {
                if (markersArray) {
                        for (var i = 0; i < markersArray.length; i++ ) {
                                markersArray[i].setMap(null);
                        }
                }
                markersArray.length = 0;
        }        

        //cargarMapa();

        //end mapa



      $("#modal_addform2").submit(function(event) {
        event.preventDefault(); 
        // get values

            var start = $("#dtp_input1").val();
            var end = $("#dtp_input2").val();

          start = new Date(start).getTime() / 1000;
          end = new Date(end).getTime() / 1000; 
          if(start < end){

            var $form = $( this ),
            title = $form.find( '#add_event_title' ).val(),
            organizer_name = $form.find( '#organizer_name' ).val(),                        
            event_uri = $form.find('#add_event_uri').val(),
            start = start,
            end = end,
            e_address = $form.find( '#event_address' ).val(),
            e_latitude = $form.find('#event_latitude').val(),
            e_longitude = $form.find('#event_longitude').val(),            
            e_description = $form.find( '#add_event_description' ).val(),
            url = $form.attr( 'action' ); 
        

        console.log("title: "+title+", organizer_name: "+organizer_name+", lat: "+e_latitude+", long: "+ e_longitude+", address: "+e_address+", start: "+start+", end: "+end+", e_description: "+e_description+", event_uri: "+event_uri);
        //throw new Error("Error intentionally created to halt process. Not an actual error.");

        // send data and get results

        $.post( url, { title: title,
                       organizer_name: organizer_name,                         
                       event_uri: event_uri, 
                       start: start,
                       end: end,
                       e_address: e_address,
                       e_lat: e_latitude, 
                       e_lng: e_longitude,
                       e_description: e_description
                        },
          function( data ) {
            var content = $( data ).find( '#content' );
            
            // if submission was successful, show info alert
            if(data == "success") {
              $("#modal_addform2 #result").html("Gracias por sumar el evento, prontó aparecerá en el mapa"); 
              $("#modal_addform2 #result").addClass("alert alert-info");
              $("#modal_addform2 p").css("display", "none");
              $("#modal_addform2 fieldset").css("display", "none");
              $("#modal_addform2 .btn-primary").css("display", "none");
              
            // if submission failed, show error
            } else {
              $("#modal_addform2 #result").html(data); 
              $("#modal_addform2 #result").addClass("alert alert-danger");
            }
          }
        );
        }else{
          alert("la fecha de Termino no puede ser mayor a la de Inicio");
        }
      });
</script>














    <!-- startup genome modal -->
    <div class="modal hide" id="modal_add_choose">
      <form action="add.php" id="modal_addform_choose" class="form-horizontal">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">×</button>
          <h3>Add something!</h3>
        </div>
        <div class="modal-body">
          <p>
            Want to add your company to this map? There are two easy ways to do that.
          </p>
          <ul>
            <li>
              <em>Option #1: Add your company to Startup Genome</em>
              <div>
                Our map pulls its data from <a href="http://www.startupgenome.com">Startup Genome</a>.
                When you add your company to Startup Genome, it will appear on this map after it has been approved.
                You will be able to change your company's information anytime you want from the Startup Genome website.
              </div>
              <br />
              <a href="http://www.startupgenome.com" target="_blank" class="btn btn-info">Sign in to Startup Genome</a>
            </li>
            <li>
              <em>Option #2: Add your company manually</em>
              <div>
                If you don't want to sign up for Startup Genome, you can still add your company to this map.
                We will review your submission as soon as possible.
              </div>
              <br />
          <a href="#modal_add" target="_blank" class="btn btn-info" data-toggle="modal" data-dismiss="modal">Submit your company manually</a>
            </li>
          </ul>
        </div>
      </form>
    </div>
    
  </body>
</html>
